package cours_1;

public class Main {

	public static void main(String[] args) {

		try {
			User user = new User("admin","azerty");
			XMLTools.encodeToFile(user, "user.xml");
			System.out.println(user);
			User user2= new User("newAdmin","123456");
			XMLTools.encodeToFile(user2, "user.xml");
			System.out.println(user);
			user = (User) XMLTools.decodeFromFile("user.xml");
			System.out.println(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
//		
//		try {
//			User user = new User();
//			user = (User) XMLTools.decodeFromFile("user.xml");
//			System.out.println(user);
//			
//		}catch(Exception e) {
//			e.printStackTrace();
//		}

	}

}
