package cours_2;

import java.util.ArrayList;
import java.util.List;



public class Main {

	public static void main(String[] args) {

		try {
			List<Adresse> adresse = new ArrayList<Adresse>();
			adresse.add(new Adresse("bd des �cureuils", "06210","Mandelieu"));
			adresse.add(new Adresse("rue de Cannes", "06210","Mandelieu"));			
			
			List<Personne> personne = new ArrayList<Personne>();
			personne.add(new Personne("Potter","Harry",adresse));
			
			XMLTools.encodeToFile(personne, "personne.xml");
			//System.out.println(personne);
		
			//List<Personne> newList = new ArrayList<Personne>();
			personne = (List<Personne>) XMLTools.decodeFromFile("personne.xml");
			System.out.println(personne);
			
		}catch(Exception e) {
			e.printStackTrace();
	}

	}
}