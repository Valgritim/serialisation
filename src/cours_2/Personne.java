package cours_2;

import java.io.Serializable;
import java.util.List;

public class Personne implements Serializable{
	
	private String nom;
	private String prenom;
	private List<Adresse> adresses;
	
	public Personne() {
		super();
	}

	public Personne(String nom, String prenom, List<Adresse> adresses) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.adresses = adresses;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<Adresse> getAdresses() {
		return adresses;
	}

	public void setAdresses(List<Adresse> adresses) {
		this.adresses = adresses;
	}

	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", adresses=" + adresses + "]";
	}
	
	

}
