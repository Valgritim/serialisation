package cours_2;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class XMLTools {
	
	
	public static void encodeToFile(Object object, String filename) throws FileNotFoundException, IOException, IntrospectionException {
		
		XMLEncoder encoder = new XMLEncoder(new FileOutputStream(filename));
		
		try {
		//Serialisation de l'objet
			encoder.writeObject(object);
			encoder.flush();
		} finally {
			
			encoder.close();
		}
	}
	
	public static Object decodeFromFile(String filename) throws FileNotFoundException, IOException{
		
		Object object = null;
		//ouverture du d�codeur:
		XMLDecoder decoder = new XMLDecoder(new FileInputStream(filename));
		
		try {
			object = decoder.readObject();
		} finally {
			//Fermeture du d�codeur
			decoder.close();
		}
		return object;
	}

}
